# shiny_shinydashboard

L'objectif de ce projet est de proposer une application intéractive permettant de choisir une molécule (et les médicaments qui y sont associés) en fonction des effets indésirables rapportés par les patients.

Les données d'effet indésirables sont disponibles sur le site https://data.ansm.sante.fr/.

Ce projet est la suite du projet de Web scraping mené par Chloé Saint-Dizier, Nargess Jafarzadeh, Chouaib El Khaoudi - Master Data Science en Santé, Université de Lille, ILIS.

# Base de données 

Les données collectées sont stockées dans 5 tables. Le schéma relationnel de la base de données est représenté ci-dessous :

<img src="./ressources/schema_database.png" width="600">


- La table **substance** est la table centrale, elle liste toutes les substances avec leur code et leur nom.

- La table **consumption** contient les nombre de délivrances entre 2014 et 2021, par genre et classe d'âge.

- La table **declaration** contient le nombre de déclarations d'effets secondaires entre 2014 et 2021, par genre et classe d'âge.

- La table **side_effect** contient le nombre de cas d'effets secondaires relevés par famille de pathologie.

- La table **drug** contient tous les médicaments associés aux substances d'intérêt.

Pour l'instant, nous ne disposons que des données relatives aux antidépresseurs mais nous serons amenés à étendre la base de données aux antipsychotiques et thymorégulateurs.

# Instructions

- Copier le script shinydashboard_basic_template.R et le renommer shinydashboard_pharmacovigilance.R
- Renommer le titre du dashboard "Pharmacovigilance"
- Changer la couleur du dashboard avec le paramètre "skin".
- Dans le menu (*sidebarMenu*), créer trois éléments : *Antidépresseurs*, *Antipsychotiques*, *Thymorégulateurs*.
- Dans le corps du tableau (*dashboardBody*), créer les trois panneaux correspondants aux éléments du menu.
- Pour chacun de ces panneaux, ajouter un titre H2 *Antidépresseurs*, *Antipsychotiques*, *Thymorégulateurs*.
- Dans le panneau *Antidépresseurs* :
	- ajouter un menu déroulant avec les intitulés des effets indésirables
	- après sélection d'un effet indésirable, afficher les 5 molécules pour lesquelles cet effet indésirables a été rapporté le moins souvent.

<img src="./ressources/maquette_1.png" width="600">

- A partir de la liste des cinq molécules, proposer un mode de sélection d'une molécule, et afficher les noms des médicaments (sans la forme galénique ni le dosage) correspondant dans un second tableau. Exemple : Sertraline, Tofranil, etc.

<img src="./ressources/maquette_2.png" width="600">